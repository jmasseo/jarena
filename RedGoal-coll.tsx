<?xml version="1.0" encoding="UTF-8"?>
<tileset name="RedGoal" tilewidth="16" tileheight="16">
 <image source="../../Desktop/gameart/RedDiamondInverse96x96Sheet.png" trans="ffffff" width="96" height="96"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="29">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="30">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="31">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="35">
  <objectgroup draworder="index">
   <object x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
