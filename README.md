# README #
Jarena!!!!
This is the README for the source Repo of Jarena.  A Joust-Arena game.

I'm open sourcing this because someone else wanted to use the idea for a sample game for their platform.
I like the game, but I got discouraged because people hated on the network lag and prediction was 'questionable' with Unity3's non-deterministic physics engine.

I did not create any of the source art.  I did create the levels.

I have not touched this in a few years so I don't really remember much about how it worked, other than it was a pretty straight forward implementation under the system at the time.

Anything that is mine(source code, levels, 'idea', whatever) is now MIT licensed.