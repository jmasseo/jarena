﻿using UnityEngine;
using System.Collections;

public class ChaseCam : MonoBehaviour
{
    public float dampTime = 0.01f; //offset from the viewport center to fix damping
    private Vector3 velocity = Vector3.zero;
    public Transform target;
    private PlayerControlledObjectScript pcos;

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        if (target)
        {
            if (pcos.alive)
            {
                Vector3 point = camera.WorldToViewportPoint(target.position);
                Vector3 delta = target.position -
                            camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
                Vector3 destination = transform.position + delta;
                transform.position = Vector3.SmoothDamp(transform.position, destination,
                                                        ref velocity, dampTime);
            }
        }
        else
        {
            try
            {
                GameObject[] targets;
                targets = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject t in targets)
                {
                        PlayerControlledObjectScript pc = t.GetComponent<PlayerControlledObjectScript>();
                        if(pc.isMyPlayer)
                        {
                        target = t.transform;
                        pcos = pc;
                        break;
                        }
                    }
                
            }
            catch (System.NullReferenceException ex)
            { return; }
        }
    }
}