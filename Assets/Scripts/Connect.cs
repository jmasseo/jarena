// CS4455 networking sample.  Blair MacIntyre, 2013.
// 
// Demonstrates a strict authoritative server, where all content is created
// by the server, and interactions between objects are only reacted to on the server (with results sent 
// to the clients via RPC calls).
//
// Also demonstrates a simple use of the game lobby for setup.
//
// Some networking content drawn from the "Ultimate Unity networking project" by M2H (http://www.M2H.nl) and 
// from http://www.paladinstudios.com/2013/07/10/how-to-create-an-online-multiplayer-game-with-unity/
//

using UnityEngine;
using System.Collections;

public class Connect : MonoBehaviour
{
	// we'll use Unity's game lobby
    private const string typeName = "jarana_07-31-2014";
    private const string gameName = "TESTSERV";
	public int connectPort = 25001;

    private bool isRefreshingHostList = false;
    private HostData[] hostList;
    private string playerName;
	private string debugText;
    public int lastLevelPrefix;
    bool launchingGame;
    public string selectedLevel;
    public string disconnectedLevel;
    public string lobbyLevel;

	void Awake()
    {
        string pname = UnityEngine.PlayerPrefs.GetString("playerName");
        if (pname == "") pname = "NewPlayer";
        playerName = pname;
        
    }
    void launchingGameGUI()
    {
        GUI.Box(new Rect(Screen.width / 4 + 180, Screen.height / 2 - 30, 280, 50), "");
        if (Application.CanStreamedLevelBeLoaded(selectedLevel))
        {
            GUI.Label(new Rect(Screen.width / 4 + 200, Screen.height / 2 - 25, 285, 150), "Loaded, starting the game! " + selectedLevel);

            LoadLevel(selectedLevel, lastLevelPrefix);
        }
        else
        {
            GUI.Label(new Rect(Screen.width / 4 + 200, Screen.height / 2 - 25, 285, 150), "Starting.... Loading the game: " + Mathf.Floor(Application.GetStreamProgressForLevel(selectedLevel) * 100) + "%");
        }
    }
	// Supports both client and server setup
	void OnGUI ()
	{
   
		if (Network.peerType == NetworkPeerType.Disconnected)
		{
			// Currently disconnected: Neither client nor server
			GUILayout.Label("Connection status: Disconnected");
		
			int.TryParse(GUILayout.TextField(connectPort.ToString()), out connectPort);
		
			GUILayout.BeginVertical();
			if (GUILayout.Button ("Start Server"))
			{
                StartServer();
			}

			if (GUILayout.Button ("Refresh Hosts"))
			{
				// Connect to the "connectToIP" and "connectPort" as entered via the GUI
                RefreshHostList();
			}
            GUILayout.Label("PlayerName:");
            
            string plName = GUILayout.TextField(playerName);
            if (plName != playerName)
            {
                playerName = plName;
                UnityEngine.PlayerPrefs.SetString("playerName", plName);
                UnityEngine.PlayerPrefs.Save();
            }
		
			if (hostList != null)
            {
                for (int i = 0; i < hostList.Length; i++)
                {
                    if (GUI.Button(new Rect(200, 25 + (35 * i), 200, 30), hostList[i].gameName))
                        JoinServer(hostList[i]);
                }
            }
			GUILayout.EndVertical();
		} else {
			// One or more connection(s)!		

			if (Network.peerType == NetworkPeerType.Connecting)
			{	
				GUILayout.Label("Connection status: Connecting");		
			} 
			else if (Network.peerType == NetworkPeerType.Client)
			{
				GUILayout.Label("Connection status: Client!");
				GUILayout.Label("Ping to server: " + Network.GetAveragePing(Network.connections[0] ) );
			} 
			else if (Network.peerType == NetworkPeerType.Server)
			{
				GUILayout.Label("Connection status: Server!");
				GUILayout.Label("Connections: " + Network.connections.Length);
				if (Network.connections.Length>=1) 
				{
					GUILayout.Label("Ping to first player: "+Network.GetAveragePing(  Network.connections[0] ) );
				}			
			}

			if (GUILayout.Button ("Disconnect"))
			{
				//GameManager GM = GameObject.Find("code").GetComponent<GameManager>();
				if (Network.isServer)
				{
					if (GameManager.SP != null) GameManager.SP.ShutDownServer();
				} else {
					if (GameManager.SP != null) GameManager.SP.ShutDownClient();
				}
				Network.Disconnect();
			}
		}
	}

    private void StartServer()
    {
        Network.InitializeServer(10, connectPort, !Network.HavePublicAddress());
		MasterServer.dedicatedServer = true;
        MasterServer.RegisterHost(typeName, playerName);
    }

    void Update()
    {
        if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
        {
            isRefreshingHostList = false;
            hostList = MasterServer.PollHostList();
        }
    }

    private void RefreshHostList()
    {
        if (!isRefreshingHostList)
        {
            isRefreshingHostList = true;
            MasterServer.RequestHostList(typeName);
        }
    }

    private void JoinServer(HostData hostData)
    {
        Network.Connect(hostData);
    }


	
	// some simple debugging
	
	// first, on the client
	void OnConnectedToServer() 
	{
		Debug.Log ("This CLIENT has connected to a server");
	}

	void OnDisconnectedFromServer(NetworkDisconnection info) 
	{
		Debug.Log("This CLIENT has disconnected from a server OR this SERVER was just shut down");
        ChangeLevel(GameManager.SP.disconnectedLevel, lastLevelPrefix);

	}

	void OnFailedToConnect(NetworkConnectionError error)
	{
		Debug.Log("Could not connect to server: "+ error);
	}

	// second, on the server
	void OnPlayerConnected(NetworkPlayer player) 
	{
		Debug.Log("Player connected from: " + player.ipAddress +":" + player.port);
	}

	void OnServerInitialized() 
	{
		Debug.Log("Server initialized and ready");
        ChangeLevel(GameManager.SP.lobbyLevel, lastLevelPrefix + 1);
	}

	void OnPlayerDisconnected(NetworkPlayer player) 
	{
		Debug.Log("Player disconnected from: " + player.ipAddress+":" + player.port);
	}

	// other network callbacks
	void OnFailedToConnectToMasterServer(NetworkConnectionError info) 
	{
		Debug.Log("Could not connect to master server: "+ info);
	}

	void OnNetworkInstantiate (NetworkMessageInfo info) 
	{
		Debug.Log("New object instantiated by " + info.sender);
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		//Custom code here (your code!)
	}
    public void ChangeLevel(string level, int levelPrefix)
    {
        //LoadLevel(level, levelPrefix);
        Network.RemoveRPCsInGroup(0);
        networkView.RPC("LoadLevel", RPCMode.AllBuffered, level, levelPrefix);
    }
    [RPC]
    public void LoadLevel(string level, int levelPrefix)
    {
        // Clear the GameManagers object lists and shit.
        GameManager.SP.playerList.Clear();
        GameManager.SP.myGOList.Clear();
        // omitted code
        Network.SetSendingEnabled(0, false);
        Network.isMessageQueueRunning = false;

        Network.SetLevelPrefix(levelPrefix);
        Application.LoadLevel(level);
        //wait till level is loaded
        new WaitForSeconds(2);

        Debug.Log(level);
        /*
        // Allow receiving data again
        Network.isMessageQueueRunning = true;
        // Now the level has been loaded and we can start sending out data
        Network.SetSendingEnabled(0, true);
        */
        // Notify our objects that the level and the network is ready
        foreach (GameObject go in FindObjectsOfType(typeof(GameObject)))
        {
            go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);
        }
    }
}