﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TeamManagerScript : MonoBehaviour {
    public Dictionary<int, List<NetworkPlayer>> teams;
    public bool team1Enabled;
    public bool team2Enabled;
    public bool team3Enabled;
    public bool team4Enabled;
    private PlayerControlledObjectScript localPcos;
    private GameObject mygo;
    private bool showTeamSelect;
    private Rect tsRect;
    public int team1Score;
    public int team2Score;
    public int team3Score;
    public int team4Score;
    public GUIText scoreBoard;
    
	// Use this for initialization
	void Start () {
        teams = new Dictionary<int, List<NetworkPlayer>>();
        mygo = GameManager.SP.playerList[Network.player].go;
        tsRect = new Rect(Screen.width / 2, Screen.height / 8, Screen.width / 4, Screen.height / 3);
        if(mygo) localPcos = mygo.GetComponent<PlayerControlledObjectScript>();
        scoreBoard = GameObject.Find("ScoreTicket").guiText;

        

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        try
        {
            if (localPcos.team == 0)
            {
                Rect win = GUI.Window(10, tsRect, RenderTeamSelect, "Team Select");
            }
        } catch (System.NullReferenceException ex)
        {
            if(Network.player in GameManager.SP.playerList)
            {
                mygo = GameManager.SP.playerList[Network.player].go;
                if(mygo) localPcos = mygo.GetComponent<PlayerControlledObjectScript>();
            }
        }
        string scoreboardString = "";
        if (team1Enabled) scoreboardString += "<color=#ff0000>RED:" + team1Score + "</color>";
        if (team2Enabled) scoreboardString += "<color=#0000ff>BLUE:" + team2Score + "</color>";
        if (team3Enabled) scoreboardString += "<color=#00ff00>GREEN:" + team3Score + "</color>";
        if (team4Enabled) scoreboardString += "<color=#ffff00>YELLOW:" + team4Score + "</color>";
        scoreBoard.text = scoreboardString;
    }
    void RenderTeamSelect(int wid)
    {
        GUILayout.BeginHorizontal();
        if (team1Enabled) if (GUILayout.Button("Team 1")) { AddPlayer(Network.player, 1, RandomBlue()); };
        if (team2Enabled) if (GUILayout.Button("Team 2")) { AddPlayer(Network.player, 2,RandomRed()); };
        if (team3Enabled) if (GUILayout.Button("Team 3")) { AddPlayer(Network.player, 3,RandomGreen()); };
        if (team4Enabled) if (GUILayout.Button("Team 4")) { AddPlayer(Network.player, 4,RandomYellow()); };
        GUILayout.EndHorizontal();

    }

    void AddPlayer(NetworkPlayer np, int team, Color nc)
    {
        _APT(np, team, nc.r, nc.g, nc.b);
        networkView.RPC("_APT", RPCMode.AllBuffered, np, team, nc.r, nc.g, nc.b);
    
    }
    [RPC]
    void _APT(NetworkPlayer np, int team, float r, float g, float b)
    {
        Color nc = new Color(r, g, b);
        Debug.Log("_APT: " + team);
        if(!teams.ContainsKey(team))
        {
            teams[team] = new List<NetworkPlayer>();
        }
        teams[team].Add(np);
        GameObject go = GameManager.SP.playerList[np].go;
        go.renderer.material.color = nc;
        GameManager.SP.playerList[np].color = nc;
        PlayerControlledObjectScript pcos;
        pcos = go.GetComponent<PlayerControlledObjectScript>();
        
        pcos.team = team;
    }

    void RemovePlayer(NetworkPlayer np, int team)
    {
        networkView.RPC("_RPT", RPCMode.AllBuffered, np, team);
    }
    [RPC]
    void _RPT(NetworkPlayer np, int team)
    {
        Debug.Log("_RPT: " + team);
        teams[team].Remove(np);
        GameObject go = GameManager.SP.playerList[np].go;
        PlayerControlledObjectScript pcos;
        pcos = go.GetComponent<PlayerControlledObjectScript>();
        pcos.team = 0;
    }

    public void ScoreTeam1()
    {
        networkView.RPC("_ScoreTeam1", RPCMode.AllBuffered);
       
    }
    public void ScoreTeam2()
    {
        networkView.RPC("_ScoreTeam2", RPCMode.AllBuffered);
    }
    public void ScoreTeam3()
    {
        networkView.RPC("_ScoreTeam3", RPCMode.AllBuffered);
    }
    public void ScoreTeam4()
    {
        networkView.RPC("_ScoreTeam4", RPCMode.AllBuffered);
    }

    [RPC]
    void _ScoreTeam1()
    {
        team1Score++;
    }
    [RPC]
    void _ScoreTeam2()
    {
        team2Score++;
    }
    [RPC]
    void _ScoreTeam3()
    {
        team3Score++;
    }
    [RPC]
    void _ScoreTeam4()
    {
        team4Score++;
    }

    public Color RandomRed()
    {
        return new Color(Random.Range(0.9f, 1f), Random.Range(0f, 0.3f), Random.Range(0f, 0.3f));
    }
    public Color RandomBlue()
    {
        return new Color(0f, Random.Range(0f, 0.3f), Random.Range(0.8f, 1f));
    }
    public Color RandomGreen()
    {
        return new Color(Random.Range(0f, 0.3f), Random.Range(0.8f, 1f), Random.Range(0f, 0.3f));
    }
    public Color RandomYellow()
    {
        return new Color(Random.Range(0.9f, 1f), Random.Range(0.9f, 1f), Random.Range(0f, 0.3f));
    }
}
