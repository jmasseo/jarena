﻿using UnityEngine;
using System.Collections;

public class TeamCameraPanColorBackground : MonoBehaviour {
    public GameObject redgoal;
    public GameObject bluegoal;
    public GameObject thecake;
	// Use this for initialization
	// Find the goals.
    void Start () {
        redgoal = GameObject.Find("RedBeacon");
        bluegoal = GameObject.Find("BlueBeacon");
        thecake = GameObject.Find("Cake");
	}
	// let's make this shit gnarly for red and blue!
	// Update is called once per frame
    // On Team Bigger - Red is right and blue is left.  So we'll 
	void Update () {
        float scale = Vector3.Distance(redgoal.transform.position,bluegoal.transform.position);
        float mrd = Vector3.Distance(transform.position,redgoal.transform.position);
        float mcd = Vector3.Distance(transform.position,thecake.transform.position);
        float mbd = Vector3.Distance(transform.position, bluegoal.transform.position);
        float crd, ccd, cbd;
        if (mrd > (scale / 3) * 2) crd = 0; else crd = 3f / mrd;
        if (mcd > scale / 2) ccd = 0; else ccd = 1f / mcd;
        if (mbd > (scale / 3) * 2) cbd = 0; else cbd = 3f / mbd;
        //Debug.Log("MRD" + mrd + " MCD " + mcd + "MBD " + mbd + " SCALE " + scale + " R " + crd + " G " + ccd + "  B " + cbd);
        this.camera.backgroundColor = new Color(crd,ccd,cbd);

	
	}
}
