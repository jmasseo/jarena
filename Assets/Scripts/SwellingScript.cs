﻿using UnityEngine;
using System.Collections;

public class SwellingScript : MonoBehaviour {
    //This is mostly copied from tut 2B 

    public float scale = 0f;
    public float offset = 0f;
    public Vector3 origScale;
    void Awake()
    {
        origScale = transform.localScale;
    }
 
    // the server modifies these game objects
    void Update()
    {
            //Only the owner can move the cube!	
            //(Ok this check is a bit overkill since we did already disable the script in Awake)	
            float noiseX = Mathf.PerlinNoise(Time.time + offset, 0) - 0.5f;
            float noiseY = Mathf.PerlinNoise(Time.time + offset, 0) - 0.5f;
            float noiseZ = Mathf.PerlinNoise(Time.time - offset, 0) - 0.5f;
            Vector3 moveDirection = new Vector3(noiseX * scale, noiseY * scale, noiseZ * scale);
            transform.localScale = origScale + moveDirection;
            //rigidbody2D.rotation = 0f;
        
    }
}
