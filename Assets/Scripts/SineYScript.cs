﻿using UnityEngine;
using System.Collections;

public class SineYScript : MonoBehaviour {
    //This is mostly copied from tut 2B 

    public float scale = 0f;
    public float offset = 0f;
    public Vector3 origScale;
    void Awake()
    {
        origScale = transform.position;
    }

    // the server modifies these game objects
    void Update()
    {
        //Only the owner can move the cube!	
        //(Ok this check is a bit overkill since we did already disable the script in Awake)	
        //float noiseX = Mathf.PerlinNoise(Time.time + offset, 0) - 0.5f;
        float noiseY = Mathf.Sin(Time.time + offset);
        //float noiseZ = Mathf.PerlinNoise(Time.time - offset, 0) - 0.5f;
        Vector3 moveDirection = new Vector3(0, noiseY * scale, 0);
        transform.position = origScale + moveDirection;
        //rigidbody2D.rotation = 0f;

    }
}
