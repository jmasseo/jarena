﻿using UnityEngine;
using System.Collections;

public class CakeBehaviour : MonoBehaviour {
    public Color cakecolor;
    public bool showarrowTeam1;
    public bool showarrowTeam2;
    public bool showarrowTeam3;
    public bool showarrowTeam4;
    public int teamOwner;
    public GameObject attachedto;
    public GameObject lastattachedto;
    private SpringJoint2D mySpring;
    public bool canCapture;

    // Use this for initialization
	void Start () {
        //renderer.material.color = cakecolor;
        if (!Network.isServer) rigidbody2D.isKinematic = true;
        mySpring = this.GetComponent<SpringJoint2D>();
        mySpring.enabled = false;
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void ChangeColor(Color nColor)
    {
        networkView.RPC("_CC",RPCMode.AllBuffered,nColor);
    }
    [RPC]
    void _CC(Color nColor)
    {
        cakecolor = nColor;
        renderer.material.color = nColor;
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (Network.isServer)
        {
            foreach (ContactPoint2D contact in collision.contacts)
            {
                if (!this.canCapture) continue;
                if (contact.collider.gameObject.name.Substring(0, 6) == "Player")
                {
                    if (attachedto)
                    {
                        lastattachedto = attachedto;
                        PlayerControlledObjectScript opcos = attachedto.GetComponent<PlayerControlledObjectScript>();
                        if (opcos.attached.Contains(this.gameObject)) opcos.attached.Remove(this.gameObject);
                    }
                    attachedto = contact.collider.gameObject;
                    mySpring.connectedBody = attachedto.rigidbody2D;
                    PlayerControlledObjectScript pcos = attachedto.GetComponent<PlayerControlledObjectScript>();
                    pcos.attached.Add(this.gameObject);
                    mySpring.enabled = true;


                }
            }
        }
    }
    //public void ConnectTo(e)
    [RPC]
    public void _AttachTo(NetworkPlayer player)
    {
        if (attachedto)
        {
            lastattachedto = attachedto;
            PlayerControlledObjectScript opcos = attachedto.GetComponent<PlayerControlledObjectScript>();
            if (opcos.attached.Contains(this.gameObject)) opcos.attached.Remove(this.gameObject);


        }
        attachedto = GameManager.SP.playerList[player].go; 
        mySpring.connectedBody = attachedto.rigidbody2D;
        PlayerControlledObjectScript pcos = attachedto.GetComponent<PlayerControlledObjectScript>();
        pcos.attached.Add(this.gameObject);
        mySpring.enabled = true;
    }

    public void DisconnectSpring()
    {
        networkView.RPC("_DisconnectSpring", RPCMode.All);
    }
    [RPC]
    public void _DisconnectSpring()
    {
        if (attachedto)
        {
            lastattachedto = attachedto;
            PlayerControlledObjectScript opcos = attachedto.GetComponent<PlayerControlledObjectScript>();
            if (opcos.attached.Contains(this.gameObject)) opcos.attached.Remove(this.gameObject);
        }

        mySpring.enabled = false;
        lastattachedto = attachedto;
        attachedto = null;
        mySpring.connectedBody = null;
    }

}
