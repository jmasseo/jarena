﻿using UnityEngine;
using System.Collections;

public class GoalBehaviourScript : MonoBehaviour
{
    public int team;
    
    public TeamManagerScript tms;
    // Use this for initialization
    void Start()
    {
        tms = GameObject.Find("TeamManager").GetComponent<TeamManagerScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("GOALZONECOLLIDE:" + this.gameObject.name + " MEETS " + other.gameObject.name);
        if (!Network.isServer) return;
        if(other.gameObject.name.Substring(0,4) == "Cake")
        {
            CakeBehaviour ck = other.gameObject.GetComponent<CakeBehaviour>();
            ck.DisconnectSpring();
            networkView.RPC("PlayBuzzer", RPCMode.All);
            switch(team)
            {
                case 1:
                    tms.ScoreTeam1();
                    break;
                case 2:
                    tms.ScoreTeam2();
                    break;
                case 3:
                    tms.ScoreTeam3();
                    break;
                case 4:
                    tms.ScoreTeam4();
                    break;
                default:
                    break;
            }
            other.gameObject.transform.position = new Vector3(0, 0, 0);
        }

    }
    [RPC]
    void PlayBuzzer()
    {
        this.audio.Play();
    }
}
