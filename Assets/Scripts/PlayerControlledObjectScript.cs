// script for a player-controlled object (their avatar, usually).
// Here we send the player movement commands to the server.  The server will then send back the proper
// position, based on the centralized game simulation.  If the network update isn't fast enough, this would result in
// sluggish movement.  Ideally, you'd want to keep some history, run locally and then jump if there are conflicts 
// (i.e., think how SecondLife does it)

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControlledObjectScript : MonoBehaviour {
	// our movement vector.
	private Vector3 moveDirection = Vector3.zero;
    private Vector3 oldmoveDirection = Vector3.zero;
	public float speed = 5f;
	private bool lastNonZero = false;
    public int kills;
    public int deaths;
    private GameManager gm;
	// for recording hits from server
	public GameObject hitCountText;
	public int hitCount = 0;
    public AudioClip hitSound;
	// who this represents, and is it me? 
	public bool isMyPlayer = false;
	public NetworkPlayer player;
    public Sprite leftMan;
    public Sprite rightMan;
    public SpriteRenderer riderSprite;
    public bool facing; // I'm facing to the right if i'm true
    public bool flapping; // flappin
    public bool walking; // walkin
    public bool stopping; // stopping
    public bool alive; // am I ALIVE
    public int team; // im on a team b----.
    public List<GameObject> holding;
    public List<GameObject> attached;
	// simple initialization
    private Animator animator;
    private LobbyChat lc;
    private Rect respawnRect;
    private TeamManagerScript tms;
    private Vector3 myVel;
    private Vector3 newPos;
    //private float lastUpdate;
    //private float updateTimes;
    //private ref GameManager.PlayerInfo mypi;
	void Start () {
        if(holding == null) holding = new List<GameObject>();
        if(attached == null) attached = new List<GameObject>();

		hitCount = 0;
        
        if(!Network.isServer)
        {
            //this.rigidbody2D.isKinematic = true;
        }
        animator = this.GetComponent<Animator>();
        GameObject go1 = GameObject.Find("Code");
        gm = go1.GetComponent<GameManager>();
        lc = go1.GetComponent<LobbyChat>();
        GameObject go2 = GameObject.Find("TeamManager");
        if(go2)
        {
            tms = go2.GetComponent<TeamManagerScript>();
            team = 0;
        }
        else { team = -1;  }
        int RRwidth = 100;
        int RRheight = 100;
        respawnRect = new Rect(Screen.width / 2 - RRwidth / 2, Screen.height/2 - RRheight + 5, RRwidth, RRheight);
        /*
         * renderer.material.color = GameManager.SP.playerList[Network.player].color;
         * gameObject. renderer.material.color = GameManager.SP.playerList[Network.player].color;
         * getChildGameObject(gameObject, "RiderSprite").renderer.material.color = GameManager.SP.playerList[Network.player].rcolor;
        */
        
        
	}
    static public GameObject getChildGameObject(GameObject fromGameObject, string withName)
    {

        Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>();
        foreach (Transform t in ts) if (t.gameObject.name == withName) return t.gameObject;
        return null;
    }
	// send any commands that modify the controlled object to the server
	void Update () {
		// only the player for whom this is there object gets to move it.
        if (isMyPlayer)
		{
			float xm = Input.GetAxis("Horizontal");
			float ym = Input.GetAxis("Vertical");
            // we will set the moveVector, apply changes immediately, then take the updates from the server as confirmation
            oldmoveDirection = moveDirection;
            moveDirection = new Vector2(xm, ym);
            LMP(new Vector2(xm, ym));
			// when we stop moving, make sure we send a (0,0,0) movement vector
            if (moveDirection != oldmoveDirection)
            {
                if (xm != 0f || ym != 0f)
                {
                    // actually execute the movement on the server, not the client!

                    networkView.RPC("movePlayer", RPCMode.Server, moveDirection);
                    lastNonZero = true;
                }
                else if (lastNonZero)
                {
                    lastNonZero = false;

                    networkView.RPC("movePlayer", RPCMode.Server, moveDirection);
                }
            }
            if (!Network.isServer)
            {
                int smooth = 3;
                
                //float val = Vector3.Distance(transform.position, newPos);
                Vector3 nnp = transform.position + myVel;
                //transform.position = Vector3.Lerp(transform.position, newPos, smooth*Time.deltaTime);
                //transform.position += (myVel * Time.deltaTime);
                //transform.position = Vector3.Lerp(transform.position,nnp,0.5f*Time.deltaTime);
            }
            //if (isMyPlayer) transform.position = newPos;
		}
			
	}
	
	// gets executed on the server and the client.  Client will move the objects, which will make things responsive, but the server 
	// will override those movement when it sends it's results down.  
	void FixedUpdate () {
        
		if (Network.isServer||true) 
		{
			if (moveDirection.magnitude> 0.001) 
			{
                
	    	    rigidbody2D.AddForce(100f * moveDirection * speed * Time.deltaTime);
                animator.speed = 0.1f + rigidbody2D.velocity.magnitude / 10;
                if (moveDirection.x > 0) facing = true; else if (moveDirection.x < 0) facing = false;
                stopping = false;
			}
                flapping = Mathf.Abs(rigidbody2D.velocity.y) > 0.2f;
                walking = (!flapping && rigidbody2D.velocity.x != 0);
                stopping = !flapping&&!walking;
                animator.SetBool("facing", facing);
                animator.SetBool("walking", walking);
                animator.SetBool("stopping", stopping);
                animator.SetBool("flapping", flapping);

		}
        rigidbody2D.rotation = 0;
        
	}
	
	// stream state changes (in this case, position, but it could be anything you want) to the clients
	void OnSerializeNetworkView ( BitStream stream ,   NetworkMessageInfo info  ){
        
		if (stream.isWriting){
			//Executed on the owner of this networkview; 
			//The server sends it's position over the network
			Vector3 pos = transform.position;		
			stream.Serialize(ref pos);//"Encode" it, and send it
            Vector3 md = moveDirection;
            stream.Serialize(ref md);
            Vector3 vel = rigidbody2D.velocity;
            stream.Serialize(ref vel);
            stream.Serialize(ref facing);
            stream.Serialize(ref walking);
            stream.Serialize(ref stopping);
            stream.Serialize(ref flapping);
            stream.Serialize(ref alive);
            stream.Serialize(ref kills);
            stream.Serialize(ref deaths);
            //stream.Serialize(ref team);
					
		} else {
			//Executed on the others; 
			//receive a position and set the object to it
			Vector3 posReceive = Vector3.zero;
			stream.Serialize(ref posReceive); //"Decode" it and receive it
            //transform.position = newPos;
            newPos = posReceive;
            if ((Vector3.Distance(transform.position, newPos) > 0.5f)||this.player != Network.player)
            {
                Debug.Log("EXCEED DISTANCE - WARp");
                transform.position = posReceive;
            }
            Vector3 dir = Vector3.zero;
            stream.Serialize(ref dir);
            Vector3 vel = Vector3.zero;
            stream.Serialize(ref vel);
            myVel = vel;
            rigidbody2D.velocity = vel;
            stream.Serialize(ref facing);
            stream.Serialize(ref walking);
            stream.Serialize(ref stopping);
            stream.Serialize(ref flapping);
            stream.Serialize(ref alive);
            stream.Serialize(ref kills);
            stream.Serialize(ref deaths);
            //stream.Serialize(ref team);

            try
            {
                animator.SetBool("facing", facing);
                animator.SetBool("walking", walking);
                animator.SetBool("stopping", stopping);
                animator.SetBool("flapping", flapping);

                animator.speed = 0.1f + vel.magnitude / 10;
            }
            catch(System.NullReferenceException ne)
                {
                }
            rigidbody2D.velocity = vel;
            LMP(dir);
            /*
            updateTimes = Time.time - lastUpdate;
            lastUpdate = Time.time;
            Debug.Log("UT: " + updateTimes + " LU: " + lastUpdate);
             */


		}
	}
	
	// use the keyboard inputs from the clients to modify the game
	[RPC]
	void movePlayer(Vector3 dir, NetworkMessageInfo info)
	{
		// save some data in the global player info struct
		GameManager.SetPlayerClickTime(info.sender, info.timestamp);
        LMP(dir);
    }
    void LMP(Vector3 dir){
        //if (dir.x > 0) facing = true; else if(dir.x < 0) facing = false;
        try
        {
            animator.SetFloat("velx", dir.x);
            animator.SetFloat("vely", dir.y);
            animator.SetFloat("oldvx", moveDirection.x);
            animator.SetFloat("oldvy", moveDirection.y);
        }
        catch (System.NullReferenceException ne)
        {
            
        }
        if (facing) riderSprite.sprite = this.rightMan; else riderSprite.sprite = this.leftMan;
        
        //animator.SetBool("flapping", dir.y != 0);
        //animator.SetBool("stopping", ((Mathf.Abs(dir.x) < Mathf.Abs(moveDirection.x)) && dir.y == 0));
        //animator.SetBool("facing", facing);
        //if(facing) riderSprite.sprite = this.rightMan; else riderSprite.sprite = this.leftMan;
        moveDirection = dir;
    }	
	
	// when the server determines the hitcount has changed, send it to everyone.
	// In our simple example, we attach a 3D Text element to each player in the server, and put a GUIText on the HUD 
	// in the client for whom this is their player.  Other players do not see this information for other players.
	//
	// So, if there is a GUIText set on this script's hitCountText variable, update it 
	[RPC]
	void updateHitCount (int newHitCount)
	{
		hitCount = newHitCount;
		if (hitCountText)
		{
			GUIText gt = hitCountText.GetComponent<GUIText>();
			if (gt) {
				gt.text = "Hit Count: " + hitCount;
			}
		}
	}

	// need to make sure we destroy the hitCountText (and any other player-specific state) when we destroy this player
	void OnDestroy()
	{
		if (hitCountText)
			Destroy (hitCountText);
	}

    public void Die()
    {
        this.deaths++;
        GameManager.PlayerInfo mypi = GameManager.SP.playerList[player];
        mypi.deaths++;
        foreach (GameObject ngo in attached)
        { 
            if(ngo.name.Substring(0,4)=="Cake")
            {
                CakeBehaviour ck = ngo.GetComponent<CakeBehaviour>();
                ck.DisconnectSpring();
            }   else
            {
                /*
                 *  Theoretical Attachment Behaviours!
                 * 
                 * 
                 */
                /*
                 AttachmentBehaviour ab = ngo.GetComponent<AttachmentBehaviour>();
                 ab.ReleaseAttachment();
                 */ 
            }
        }
        /*
        foreach (GameObject ngo in holding)
        {
            /* Theoretical Holding Behaviour */
            /*
             HoldingBehaviour hb = ngo.GetComponent<HoldingBehaviour>();
             hb.Drop(transform.position);
             *//*
            
        }*/
        this.rigidbody2D.isKinematic = true;
        // Drop Tombstone
        gm.DropTombstoneByGo(this.gameObject);
        transform.position = new Vector3(-999, -999, -999);
        this.alive = false;
    }
    public void OnGUI()
    {
        if (!this.alive && this.isMyPlayer && this.team != 0)
        {
            Rect win = GUI.Window(7, respawnRect, RespawnButton, "Respawn");
        }
    }
     void RespawnButton(int id)
    {
        if (GUILayout.Button("RESPAWN"))
        {
            if (Network.isServer) Respawn();
            networkView.RPC("Respawn", RPCMode.Server);
        }
    }
    [RPC]
    public void Respawn()
    {
        GameObject[] spawnPoints;
        if (team>0)
        {
             spawnPoints = GameObject.FindGameObjectsWithTag("Team" + team);
        }
        else
        {
             spawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
            
        }
        GameObject theGO = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Vector3 pos = theGO.transform.position;
        Quaternion rot = theGO.transform.rotation;
        transform.position = pos;
        this.alive = true;
        rigidbody2D.isKinematic = false;
    }
	// in our simple example, we just keep track of the number of collisions.  When we hit another player, we deduct a 
	// hit, when we hit one of the 3 cubes, we increment a hit.  We add or substract one point per contact point.
	//
	// this code only gets run on the server.  We also update the server's TextMesh display here.
    void OnCollisionEnter2D(Collision2D collision) 
	{
		if (Network.isServer)
		{
			int newHits = 0;
	        
            foreach (ContactPoint2D contact in collision.contacts) {
                if (!this.alive) continue;
                if (contact.collider.gameObject.name.Substring(0, 4) == "Cake") continue;
				
				if (contact.collider.gameObject.name.Substring(0,6) == "Player") {
					
					newHits --;
                    
                    Debug.Log("COLLISION: Other: " + contact.otherCollider.gameObject.name + " Ours:" + contact.collider.gameObject.name);
                    GameObject gob = contact.collider.gameObject;
                    Vector3 pos = gob.transform.position;
                    PlayerControlledObjectScript pcos = gob.GetComponent<PlayerControlledObjectScript>();
                    if (!pcos.alive) continue;
                    if ((transform.position.x > pos.x && !facing && !pcos.facing) /* to the right and facing left and opp facing left */ || (transform.position.x < pos.x && facing && pcos.facing) /* to the left and facing right and opp facing right */ )
                    {
                        audio.PlayOneShot(hitSound);
                        networkView.RPC("PlayHitSound", RPCMode.All);
                        //Debug.Log("Against the Rear");
                        this.kills++;
                        GameManager.PlayerInfo mypi = GameManager.SP.playerList[player];
                        mypi.kills++;
                        pcos.Die();
                        lc.addGameChatMessage(GameManager.SP.playerList[player].name + " puts his lance in " + GameManager.SP.playerList[pcos.player].name + " from behind...");
                    }
                    else if (transform.position.y > pos.y) /* Above */
                    {
                        if ((transform.position.x > pos.x && !facing) /* to the right and facing left */ || (transform.position.x < pos.x && facing) /* to the left and facing right */ )
                        {
                            //Debug.Log("Above and facing");
                            audio.PlayOneShot(hitSound);
                            networkView.RPC("PlayHitSound", RPCMode.All);
                            this.kills++;
                            pcos.Die();
                            lc.addGameChatMessage(GameManager.SP.playerList[player].name + " has impaled " + GameManager.SP.playerList[pcos.player].name + "!!!");

                        }
                    }
                    rigidbody2D.AddForce(contact.normal * 100f);
                    //Debug.Log("Normal X:" + contact.normal.x + " Y: " + contact.normal.y);

				} else if (contact.collider.gameObject.name.Substring(0,4) == "tomb") {
                    if(contact.normal.y == -1 && contact.normal.x == 0)
                    {
                        this.Die();
                        DynamicObjectScript dso = contact.collider.gameObject.GetComponent<DynamicObjectScript>();
                        lc.addGameChatMessage(GameManager.SP.playerList[player].name + " was crushed under " + dso.playerName + "'s tombstone.");
                    }
					//rigidbody2D.AddForce (contact.normal * 500f);
					//newHits ++;
				} else {
					//Debug.Log ("Collided with " + contact.otherCollider.gameObject.name);
				}
			}
			
			if (newHits > 0)
			{
				hitCount += newHits;
				TextMesh tm = hitCountText.GetComponent<TextMesh>();
				tm.text = "Player " + player.ToString() + "\n" + hitCount.ToString();
			
				//networkView.RPC  ("updateHitCount", RPCMode.AllBuffered, hitCount);
			}
		} 
	}
    [RPC]
    void PlayHitSound()
    {

        audio.PlayOneShot(hitSound);
    }
    public void ChangeColor(Color nColor)
    {
        networkView.RPC("_CC", RPCMode.AllBuffered, nColor);
    }
    public void ChangeRiderColor(Color nColor)
    {
        networkView.RPC("_CRC", RPCMode.AllBuffered, nColor);
    }
    [RPC]
    void _CRC(Color nColor)
    {
        GameManager.SP.playerList[player].rcolor = nColor;
        getChildGameObject(gameObject,"RiderSprite").renderer.material.color = nColor;

    }
    [RPC]
    void _CC(Color nColor)
    {
        GameManager.SP.playerList[player].color = nColor;
        renderer.material.color = nColor;
    }
}